// Walls.cpp

#include "stdafx.h"
#include <iostream>
#include "Walls.h"


Walls::Walls()
{

}

Walls::Walls(Box2DManager* box2dmanager)
{
	m_position = sf::Vector2f(0.0f, 0.0f);
	m_width = 0;
	m_height = 0;
	m_body = nullptr;
	m_box2d_manager = box2dmanager;
}

Walls::~Walls(void)
{

}

void Walls::Cleanup()
{

}

void Walls::Update(float deltatime)
{
	m_position = sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);

}


sf::Vector2f Walls::GetPosition()
{
	return m_position;
}

void Walls::SetPosition(sf::Vector2f newPos)
{
	m_position = newPos;
}

void Walls::AddWall(sf::Vector2f position, int width, int height)
{
	m_walls.push_back(new Walls(m_box2d_manager));
	m_walls[m_walls.size() - 1]->SetPosition(position);
	m_walls[m_walls.size() - 1]->SetBody(m_box2d_manager->CreateStaticBody(position.x, -position.y, height, width));
	m_height = height;
	m_width = width;
}

b2Body* Walls::GetBody()
{
	return m_body;
}

void Walls::SetBody(b2Body* newbody)
{
	m_body = newbody;
}