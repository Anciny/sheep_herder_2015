// Sprite.cpp

#include "stdafx.h"

#include "Sprite.h"

Sprite::Sprite()
{

}

Sprite::Sprite(sf::Texture* sprite_texture, int sprite_width, int sprite_height)
{
	width = sprite_width;
	height = sprite_height;
	spritetexture = sprite_texture;
	x = 0;
	y = 0;
	image = new sf::Sprite(*spritetexture, sf::IntRect(x, y, width, height));
}

void Sprite::Update(float deltatime)
{

}

sf::Sprite* Sprite::getSprite()
{
	return image;
}