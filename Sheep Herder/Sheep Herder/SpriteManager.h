// SpriteManager.h

#pragma once

#include <map>
#include <string>
#include "Sprite.h"

class SpriteManager
{
public:
	SpriteManager();

	bool Initialise(const std::string &directory);
	void Cleanup();

	Sprite* Load(const std::string &filename, int width, int height);
	void LoadTexture(const std::string &filename);

private:
	bool LoadImage(const std::string &filename);

	std::string directory;
	std::map<std::string, sf::Texture*>sprites;
};