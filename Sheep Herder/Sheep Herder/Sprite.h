// Sprite.h

#pragma once

class Sprite
{
public:
	Sprite();

	Sprite(sf::Texture* sprite_texture, int width, int height);

	sf::Sprite* getSprite();

	void Update(float deltatime);

private:
	sf::Sprite* image;
	sf::Texture* spritetexture;
	float x, y;
	int width, height;
};