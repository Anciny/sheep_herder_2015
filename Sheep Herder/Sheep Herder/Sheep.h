// Sheep.h

#pragma once

class Sheep {

public:
	Sheep();
	Sheep(sf::Vector2f position, int width, int height, std::string color);
	~Sheep(void);
	void Cleanup();

	void Update(float deltatime);

	void AddSprite(std::string sprite_name, Sprite* sheep_sprite);
	sf::Sprite* GetCurrentSprite();

	void Move();
	sf::Vector2f GetPosition();
	void SetPosition(sf::Vector2f newPos);
	sf::Vector2f GetVelocity();
	void SetVelocity(sf::Vector2f newVelo);

	b2Body* GetBody();
	void SetBody(b2Body* newbody);

	std::string GetColor();

	void ALittleMovement();

	void Observation();
	void Planning();
	void Action();

private:
	Sprite* m_current_sprite;
	std::map<std::string, Sprite*> m_sprites;

	sf::Vector2f m_position;
	int m_width;
	int m_height;
	sf::Vector2f m_velocity;

	float m_rand_x;
	float m_rand_y;

	std::string m_color;

	sf::RenderWindow *m_window;

	b2Body* m_body;


	int m_threat_level;
	int m_comfort_level;
	int m_edginess_level;
};