// Walls.h

#pragma once
#include "Box2DManager.h"


class Walls {

public:
	Walls();
	Walls(Box2DManager* box2dmanager);
	~Walls(void);

	void Cleanup();
	void Update(float deltatime);

	sf::Vector2f GetPosition();
	void SetPosition(sf::Vector2f newPos);

	void AddWall(sf::Vector2f position, int width, int height);

	b2Body* GetBody();
	void SetBody(b2Body* newbody);

	std::vector<Walls*> m_walls;

private:
	Box2DManager* m_box2d_manager;

	sf::Vector2f m_position;
	int m_width;
	int m_height;

	b2Body* m_body;
};