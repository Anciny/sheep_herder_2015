// Math.h

#pragma once

class Math
{
public:
	static sf::Vector2f Normalize(const sf::Vector2f& source);

	static float GetDistance(sf::Vector2f& fromHere, sf::Vector2f& toHere);
};