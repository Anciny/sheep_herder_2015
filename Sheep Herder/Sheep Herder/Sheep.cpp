// Sheep.cpp

#include "stdafx.h"
#include <iostream>
#include "Sheep.h"
#include <random>
#include <ctime>
#include <cstdlib>


Sheep::Sheep()
{

}

Sheep::Sheep(sf::Vector2f position, int width, int height, std::string color)
{
	m_position = position;
	m_width = width;
	m_height = height;
	m_velocity = sf::Vector2f(0.0f, 0.0f);
	m_current_sprite = nullptr;
	m_body = nullptr;
	m_color = color;
	m_rand_x = 0.0f;
	m_rand_y = 0.0f;

	m_threat_level = 0;
	m_comfort_level = 0;
	m_edginess_level = 0;
}

Sheep::~Sheep(void)
{

}

void Sheep::Cleanup()
{

}

void Sheep::Update(float deltatime)
{
	sf::Vector2f linvec = sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);
	m_current_sprite->getSprite()->setPosition(linvec);
	m_position = m_current_sprite->getSprite()->getPosition();
}

void Sheep::AddSprite(std::string sprite_name, Sprite* sheep_sprite)
{
	m_sprites.insert(std::pair<std::string, Sprite*>(sprite_name, sheep_sprite));
	if (m_current_sprite == nullptr)
	{
		auto it = m_sprites.find(sprite_name);
		m_current_sprite = it->second;
		m_current_sprite->getSprite()->setPosition(m_position);	
		m_current_sprite->getSprite()->setOrigin(m_current_sprite->getSprite()->getTexture()->getSize().x / 2, m_current_sprite->getSprite()->getTexture()->getSize().y / 2);

	}
}

sf::Sprite* Sheep::GetCurrentSprite()
{
	sf::Sprite* thisSprite = m_current_sprite->getSprite();
	return thisSprite;
}

void Sheep::Move()
{
	b2Vec2 vec = b2Vec2(m_velocity.x, -m_velocity.y);
	m_body->SetLinearVelocity(vec);
	sf::Vector2f linvec = sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);
	m_current_sprite->getSprite()->setPosition(linvec);
}

sf::Vector2f Sheep::GetPosition()
{
	return m_position;
}

void Sheep::SetPosition(sf::Vector2f newPos)
{
	m_position = newPos;
}

sf::Vector2f Sheep::GetVelocity()
{
	return m_velocity;
}

void Sheep::SetVelocity(sf::Vector2f newVelo)
{
	m_velocity = newVelo;
}

b2Body* Sheep::GetBody()
{
	return m_body;
}

void Sheep::SetBody(b2Body* newbody)
{
	m_body = newbody;
}

std::string Sheep::GetColor()
{
	return m_color;
}

void Sheep::ALittleMovement()
{
	int x = ((rand() % 2000) - 1000);
	int y = ((rand() % 2000) - 1000);

	m_rand_x = x / 1000.0f;
	m_rand_y = y / 1000.0f;

	SetVelocity(sf::Vector2f((m_velocity.x + m_rand_x), (m_velocity.y + m_rand_y)));
}


void Sheep::Observation()
{
	/*Observe local herd
		Set nearest sheep cluster
		or
		Set local herd
	Observe dogs
		Set position
		Set direction
		Set speed
	Feel dog threat // Closer dogs are at a higher threat level
	Feel comfort // Higher depending on amount of nearby sheep
	Feel edginess // Cannot be lower than highest threat, lowers each update with comfort*/
}

void Sheep::Planning()
{
	/*Calculate speed // Speed increases with edginess
	Calculate flee direction
		Calculate dog alignment // Sheep have same direction as dog if dog does not have a higher speed than sheep
		Calculate dog tangent // Sheep flee in the direction of the tangent to the dogs direction if the dog is sprinting
	Calculate local herd alignment // Sheep try to have same direction as the rest of the nearby sheep
	Calculate local herd cohesion // Sheep try to stay close to the nearby sheep
	Calculate final movement direction // Flee direction increases with threat, alignment and cohesion decreases with edginess*/
}

void Sheep::Action()
{
	/*Check collision with movement and speed
	Move with movement and new speed*/
}




/*Update observation
	Observe local herd
		Set nearest sheep cluster
		or
		Set local herd
	Observe dogs
		Set position
		Set direction
		Set speed
	Feel dog threat // Closer dogs are at a higher threat level
	Feel comfort // Higher depending on amount of nearby sheep
	Feel edginess // Cannot be lower than highest threat, lowers each update with comfort
Update planning
	Calculate speed // Speed increases with edginess
	Calculate flee direction
		Calculate dog alignment // Sheep have same direction as dog if dog does not have a higher speed than sheep
		Calculate dog tangent // Sheep flee in the direction of the tangent to the dogs direction if the dog is sprinting
	Calculate local herd alignment // Sheep try to have same direction as the rest of the nearby sheep
	Calculate local herd cohesion // Sheep try to stay close to the nearby sheep
	Calculate final movement direction // Flee direction increases with threat, alignment and cohesion decreases with edginess
Update action
	Check collision with movement and speed
	Move with movement and new speed*/