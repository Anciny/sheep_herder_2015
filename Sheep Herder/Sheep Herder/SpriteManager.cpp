//SpriteManager.cpp

#include "stdafx.h"
#include "SpriteManager.h"
#include <iostream>

SpriteManager::SpriteManager()
{

}

bool SpriteManager::Initialise(const std::string &img_directory)
{
	directory = img_directory;

	return true;
}

void SpriteManager::Cleanup()
{
	std::map<std::string, sf::Texture*>::iterator it = sprites.begin();
	while (it != sprites.end())
	{
		delete it->second;
		it->second = nullptr;
		++it;
	}

	sprites.clear();
}

Sprite* SpriteManager::Load(const std::string &filename, int width, int height)
{
	std::map<std::string, sf::Texture*>::iterator it = sprites.find(filename);
	if (it == sprites.end())
	{
		if (!LoadImage(filename))
		{
			return nullptr;
		}

		it = sprites.find(filename);
	}

	Sprite* new_sprite = new Sprite(it->second, width, height);

	return new_sprite;
}

void SpriteManager::LoadTexture(const std::string &filename)
{
	std::map<std::string, sf::Texture*>::iterator it = sprites.find(filename);
	if (it == sprites.end())
	{
		if (!LoadImage(filename))
		{
			std::cout << filename << " is not a valid picture." << std::endl;
		}
	}
}

bool SpriteManager::LoadImage(const std::string &filename)
{
	std::string path = directory + filename;
	sf::Texture* texture = new sf::Texture();
	texture->loadFromFile(path);
	if (texture == nullptr)
	{
		return false;
	}

	sprites.insert(std::pair<std::string, sf::Texture*>(filename, texture));
	return true;
}