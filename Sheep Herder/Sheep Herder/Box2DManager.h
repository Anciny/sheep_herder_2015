// Box2DManager.h

#include <Box2D/Box2D.h>
#include <stdio.h>

#pragma once
class Box2DManager {
public:
	Box2DManager();
	~Box2DManager();
	void Initialize();

	b2Body* CreateDynamicBody(float32 position_x, float32 position_y, float32 height, float32 width);

	b2Body* CreateStaticBody(float32 position_x, float32 position_y, float32 height, float32 width);

	void Update(float deltatime);
	void Draw();

private:
	b2Vec2 gravity;
	b2World* world;

	b2BodyDef bodyDef;
	b2Body* body;
	b2PolygonShape dynamicBox;

	b2FixtureDef fixtureDef;

	float32 timeStep;
	int32 velocityIterations;
	int32 positionIterations;
};