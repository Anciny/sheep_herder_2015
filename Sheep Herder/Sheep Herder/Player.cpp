#include "stdafx.h"
#include <iostream>
#include "Player.h"




Player::Player(sf::RenderWindow *setWindow, SpriteManager* sprite_manager)
{
	m_window = setWindow;
	m_position = sf::Vector2f(50.0f, 50.0f);
	m_current_sprite = nullptr;

	m_player_sprite = "Player.png";
	m_sprite_name = "Player Sprite";

	m_body = nullptr;

	m_sprite_manager = sprite_manager;
	m_width = 50;
	m_height = 50;

	m_velocity = b2Vec2(0.0f, 0.0f);
	m_speed = 10;
	
	AddSprite(m_sprite_name, m_sprite_manager->Load(m_player_sprite, m_width, m_height));

	m_current_sprite->getSprite()->setOrigin(m_current_sprite->getSprite()->getTexture()->getSize().x / 2, m_current_sprite->getSprite()->getTexture()->getSize().y / 2);
}

Player::~Player(void)
{

}

void Player::Update(float deltatime)
{
	m_current_sprite->getSprite()->setRotation(RotatePlayer());

	b2Vec2 movement(0, 0);
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		movement.y -= m_speed;
		Move(movement);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		movement.x -= m_speed;
		Move(movement);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		movement.y += m_speed;
		Move(movement);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		movement.x += m_speed;
		Move(movement);
	}
	if (movement == b2Vec2(0, 0))
	{
		m_body->SetLinearVelocity(b2Vec2(0, 0));
	}

	m_position = sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);
}

void Player::AddSprite(std::string sprite_name, Sprite* player_sprite)
{
	m_sprites.insert(std::pair<std::string, Sprite*>(sprite_name, player_sprite));
	if (m_current_sprite == nullptr)
	{
		auto it = m_sprites.find(sprite_name);
		m_current_sprite = it->second;
		m_current_sprite->getSprite()->setPosition(m_position);
	}
}

sf::Sprite* Player::GetCurrentSprite()
{
	sf::Sprite* thisSprite = m_current_sprite->getSprite();
	return thisSprite;
}


void Player::Draw() 
{
	m_window->draw(*GetCurrentSprite());
}

void Player::Move(b2Vec2 linearvelocity)
{
	b2Vec2 vec = b2Vec2(linearvelocity.x, -linearvelocity.y);
	m_body->SetLinearVelocity(vec);
	sf::Vector2f linvec = sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);
	m_current_sprite->getSprite()->setPosition(linvec);
	m_velocity = linearvelocity;
}

sf::Vector2f Player::GetPosition() 
{
	return m_position;
}

b2Body* Player::GetBody()
{
	return m_body;
}

void Player::SetBody(b2Body* newbody)
{
	m_body = newbody;
}

float Player::RotatePlayer()
{
	sf::Vector2i position = sf::Vector2i(sf::Mouse::getPosition(*m_window));
	position = sf::Vector2i(m_window->mapPixelToCoords(position));
	float angle = atan2(position.y - GetPosition().y, position.x - GetPosition().x);

	return angle * (180.0f / (atan(1) * 4));
}
