#pragma once

#include "Managers\StateManager.h"
#include "Managers\TextureManager.h"
#include "Math.h"

class Engine {
public:
	Engine();
	~Engine();

	void Initialize();
	void Run();
	void Shutdown();
private:
	sf::RenderWindow *m_window;

	TextureManager *m_texture_manager;
	StateManager *m_state_manager;

	sf::Clock m_clock;
	sf::Time m_time;
	float m_deltatime;
};