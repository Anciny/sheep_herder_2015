// Math.cpp

#include "stdafx.h"
#include <iostream>
#include "Math.h"


sf::Vector2f Math::Normalize(const sf::Vector2f& source)
{
	float length = sqrt((source.x * source.x) + (source.y * source.y));
	if (length != 0)
		return sf::Vector2f(source.x / length, source.y / length);
	else
		return source;
}

float Math::GetDistance(sf::Vector2f& fromHere, sf::Vector2f& toHere)
{
	float x1 = fromHere.x;
	float y1 = fromHere.y;
	float x2 = toHere.x;
	float y2 = toHere.y;
	float distance = sqrt(powf(x2 - x1, 2) + powf(y2 - y1, 2));
	return distance;
}