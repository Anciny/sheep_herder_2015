// FlockManager.h

#pragma once
#include "Sheep.h"
#include "Box2DManager.h"
#include "Player.h"

class FlockManager {

public:
	FlockManager(sf::RenderWindow *, SpriteManager* sprite_manager, Box2DManager* box2dmanager, Player* m_player);
	~FlockManager(void);

	void Update(float deltatime);
	void Draw();

	void AddSheep(sf::Vector2f position, std::string sheep_color);

	void Flocking(Sheep* current_sheep);
	void Move(sf::Vector2f);

	std::vector<Sheep*> m_sheep;

	sf::Vector2f ComputeAlignment(Sheep* thisSheep);
	sf::Vector2f ComputeCohesion(Sheep* thisSheep);
	sf::Vector2f ComputeSeparation(Sheep* thisSheep);

	sf::Vector2f FleeFromPlayer(Sheep* current_sheep);

private:

	SpriteManager* m_sprite_manager;
	Box2DManager* m_box2d_manager;

	float alignmentWeight;
	float cohesionWeight;
	float separationWeight;

	float fleeWeight;

	std::string m_blue_sprite;
	std::string m_pink_sprite;

	float m_start_pos_x;
	float m_start_pos_y;

	const float m_personal_space = 50.0f;
	const float m_group_distance = 100.0f;

	int m_height;
	int m_width;

	Player* m_player;

	sf::RenderWindow *m_window;
};