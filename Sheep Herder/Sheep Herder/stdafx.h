#pragma once

#include <stdio.h>
#include <tchar.h>

#include <sdkddkver.h>
#include <vector>
#include <iostream>

#pragma warning(disable:4099)
#define SFML_STATIC
#if defined(_DEBUG) 
#pragma comment(lib, "Debug/sfml-window-s-d.lib")
#pragma comment(lib, "Debug/sfml-graphics-s-d.lib")
#pragma comment(lib, "Debug/sfml-system-s-d.lib")
#pragma comment(lib, "Debug/sfml-audio-s-d.lib")
#else 
#pragma comment(lib, "Release/sfml-window-s.lib")
#pragma comment(lib, "Release/sfml-graphics-s.lib")
#pragma comment(lib, "Release/sfml-system-s.lib")
#pragma comment(lib, "Release/sfml-audio-s.lib")
#endif

#if defined(_DEBUG) 
#pragma comment(lib, "Box2D-d.lib")
#else 
#pragma comment(lib, "Box2D.lib")
#endif


#ifdef SFML_STATIC
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "freetype.lib")
#pragma comment(lib, "jpeg.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "gdi32.lib") 
#endif

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <Box2D.h>
#include "SpriteManager.h"