#include "stdafx.h"
#include "Engine.h"
#include <ctime>
#include <cstdlib>


int _tmain(int argc, _TCHAR* argv[]) {

	srand((unsigned)std::time(0));

	Engine engine;

	engine.Initialize();
	engine.Run();
	engine.Shutdown();

	return 0;
}