// Box2DManager.cpp

#include "stdafx.h"
#include <iostream>
#include "Box2DManager.h"
#include "Math.h"

//Bra tutorial sida: http://www.iforce2d.net/b2dtut/fixtures

Box2DManager::Box2DManager()
{

}

Box2DManager::~Box2DManager()
{
	if (world)
	{
		delete world;
		world = nullptr;
	}
}

void Box2DManager::Initialize()
{
	gravity = b2Vec2(0.0f, 0.0f);
	world = new b2World(gravity);

	timeStep = 1.0f / 60.0f;
	velocityIterations = 6;
	positionIterations = 2;
}

b2Body* Box2DManager::CreateDynamicBody(float32 position_x, float32 position_y, float32 height, float32 width)
{
	bodyDef.type = b2_dynamicBody;
	//b2_dynamicBody if you want the body to move in response to forces.
	bodyDef.position.Set(position_x, position_y);
	body = world->CreateBody(&bodyDef);	

	dynamicBox.SetAsBox(width, height);

	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 100.0f;
	fixtureDef.friction = 0.8f;
	//A dynamic body should have at least one fixture with a non-zero density.

	body->SetLinearDamping(0.5f);

	body->CreateFixture(&fixtureDef);

	return body;
}


b2Body* Box2DManager::CreateStaticBody(float32 position_x, float32 position_y, float32 height, float32 width)
{
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(position_x, position_y);
	body = world->CreateBody(&bodyDef);

	dynamicBox.SetAsBox(width, height);

	fixtureDef.shape = &dynamicBox;
	fixtureDef.friction = 0.0f;

	body->SetLinearDamping(0.5f);

	body->CreateFixture(&fixtureDef);

	return body;
}

void Box2DManager::Update(float deltatime)
{
	// Instruct the world to perform a single step of simulation.
	// It is generally best to keep the time step and iterations fixed.
	world->Step(timeStep, velocityIterations, positionIterations);

	// When the world destructor is called, all bodies and joints are freed. This can
	// create orphaned pointers, so be careful about your world management.
}

void Box2DManager::Draw()
{
}