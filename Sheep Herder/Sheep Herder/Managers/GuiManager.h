#pragma once

class GuiManager {
public:
	GuiManager();
	~GuiManager();

	void Draw(sf::RenderWindow *window);

	void Add_gui(const std::string &name, sf::Texture *texture);
	void Add_gui(const std::string &name, sf::Texture *texture, const sf::Vector2f &position);
	void Add_gui(const std::string &name, sf::Texture *texture, const sf::IntRect &rectangle);
	void Add_gui(const std::string &name, sf::Texture *texture, const sf::Vector2f &position, const sf::IntRect &rectangle);

	void Set_position(const std::string &name, const sf::Vector2f &position);
	void Set_rectangle(const std::string &name, const sf::IntRect &rectangle);

	sf::Vector2f Get_position(const std::string &name);
private:
	std::map<std::string, sf::Sprite> m_gui_objects;
};