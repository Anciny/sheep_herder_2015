#include "../stdafx.h"
#include "TextureManager.h"

TextureManager::TextureManager() {
}
TextureManager::~TextureManager() {
	for (std::map<std::string, sf::Texture *>::iterator itr = m_textures.begin(); itr != m_textures.end(); itr++) {
		delete (*itr).second;
		(*itr).second = nullptr;
	}
	m_textures.clear();
}

sf::Texture *TextureManager::Load_texture(const std::string &p_filename) {
	for (std::map<std::string, sf::Texture *>::iterator itr = m_textures.begin(); itr != m_textures.end(); itr++) {
		if (p_filename == (*itr).first)
			return (*itr).second;
	}

	sf::Texture *texture = new sf::Texture;
	texture->loadFromFile("../data/images/" + p_filename);

	m_textures.insert(std::pair<std::string, sf::Texture *>(p_filename, texture));

	return texture;
}
sf::Texture *TextureManager::Load_texture(const std::string &p_filename, const sf::IntRect &p_rectangle) {
	for (std::map<std::string, sf::Texture *>::iterator itr = m_textures.begin(); itr != m_textures.end(); itr++) {
		if (p_filename == (*itr).first)
			return (*itr).second;
	}

	sf::Texture *texture = new sf::Texture;
	texture->loadFromFile("../data/images/" + p_filename, p_rectangle);

	m_textures.insert(std::pair<std::string, sf::Texture *>(p_filename, texture));

	return texture;
}