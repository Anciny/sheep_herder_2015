#include "../stdafx.h"
#include "StateManager.h"

#include "../States/MainState.h"
#include "../States/GameState.h"


StateManager::StateManager(sf::RenderWindow *p_window, TextureManager *p_texture_manager) {
	m_window = p_window;

	m_texture_manager = p_texture_manager;

	m_states.insert(std::pair<std::string, State *>("Main", new MainState(m_window, m_texture_manager)));
	m_states.insert(std::pair<std::string, State *>("Game", new GameState(m_window, m_texture_manager)));

	m_current_state = nullptr;
}
StateManager::~StateManager(void) {
	for (std::map<std::string, State *>::iterator itr = m_states.begin(); itr != m_states.end(); itr++) {
		delete (*itr).second;
		(*itr).second = nullptr;
	}
	m_states.clear();

	m_current_state = nullptr;
}

bool StateManager::Update(const float &p_deltatime) {
	if (m_current_state != nullptr && m_current_state->Update(p_deltatime)) {
		std::string next_state = m_current_state->Get_next();
		m_current_state->Shutdown();
		m_current_state = nullptr;

		Set_state(next_state);
	}

	if (m_current_state == nullptr)
		return true;
	return false;
}
void StateManager::Draw() {
	if (m_current_state != nullptr)
		m_current_state->Draw();
}

void StateManager::Set_state(const std::string &p_state) {
	for (std::map<std::string, State *>::iterator itr = m_states.begin(); itr != m_states.end(); itr++) {
		if ((*itr).first == p_state) {
			if ((*itr).second->Initialize())
				m_current_state = (*itr).second;
			return;
		}
	}
}