#include "../stdafx.h"
#include "GuiManager.h"

GuiManager::GuiManager() {
}
GuiManager::~GuiManager() {
}

void GuiManager::Draw(sf::RenderWindow *p_window) {
	for (std::map<std::string, sf::Sprite>::iterator itr = m_gui_objects.begin(); itr != m_gui_objects.end(); itr++) {
		p_window->draw((*itr).second);
	}
}

void GuiManager::Add_gui(const std::string &p_name, sf::Texture *p_texture) {
	m_gui_objects.insert(std::pair<std::string, sf::Sprite>(p_name, sf::Sprite(*p_texture)));
}
void GuiManager::Add_gui(const std::string &p_name, sf::Texture *p_texture, const sf::Vector2f &p_position) {
	sf::Sprite sprite = sf::Sprite(*p_texture);
	sprite.setPosition(p_position);
	m_gui_objects.insert(std::pair<std::string, sf::Sprite>(p_name, sprite));
}
void GuiManager::Add_gui(const std::string &p_name, sf::Texture *p_texture, const sf::IntRect &p_rectangle) {
	m_gui_objects.insert(std::pair<std::string, sf::Sprite>(p_name, sf::Sprite(*p_texture, p_rectangle)));
}
void GuiManager::Add_gui(const std::string &p_name, sf::Texture *p_texture, const sf::Vector2f &p_position, const sf::IntRect &p_rectangle) {
	sf::Sprite sprite = sf::Sprite(*p_texture, p_rectangle);
	sprite.setPosition(p_position);
	m_gui_objects.insert(std::pair<std::string, sf::Sprite>(p_name, sprite));
}

void GuiManager::Set_position(const std::string &p_name, const sf::Vector2f &p_position) {
	for (std::map<std::string, sf::Sprite>::iterator itr = m_gui_objects.begin(); itr != m_gui_objects.end(); itr++) {
		if (p_name.compare((*itr).first) == 0) {
			(*itr).second.setPosition(p_position);
			return;
		}
	}
}
void GuiManager::Set_rectangle(const std::string &p_name, const sf::IntRect &p_rectangle) {
	for (std::map<std::string, sf::Sprite>::iterator itr = m_gui_objects.begin(); itr != m_gui_objects.end(); itr++) {
		if (p_name.compare((*itr).first) == 0) {
			(*itr).second.setTextureRect(p_rectangle);
			return;
		}
	}
}

sf::Vector2f GuiManager::Get_position(const std::string &p_name) {
	for (std::map<std::string, sf::Sprite>::iterator itr = m_gui_objects.begin(); itr != m_gui_objects.end(); itr++) {
		if (p_name.compare((*itr).first) == 0) {
			return (*itr).second.getPosition();
		}
	}
}