#pragma once

#include "TextureManager.h"

#include "../States/State.h"

class StateManager {
public:
	StateManager(sf::RenderWindow *window, TextureManager *texture_manager);
	~StateManager(void);

	bool Update(const float &deltatime);
	void Draw();

	void Set_state(const std::string &state);
private:
	sf::RenderWindow *m_window;

	TextureManager *m_texture_manager;

	std::map<std::string, State *> m_states;
	State *m_current_state;
};