#pragma once

class TextureManager {
public:
	TextureManager(void);
	~TextureManager(void);

	sf::Texture *Load_texture(const std::string &filename);
	sf::Texture *Load_texture(const std::string &filename, const sf::IntRect &rectangle);
private:
	std::map<std::string, sf::Texture *> m_textures;
};