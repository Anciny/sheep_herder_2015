#include "../stdafx.h"
#include "MainState.h"

MainState::MainState(sf::RenderWindow *p_window, TextureManager *p_texture_manager) {
	m_window = p_window;
	m_texture_manager = p_texture_manager;
	m_gui_manager = new GuiManager();
}
MainState::~MainState(void) {
}

bool MainState::Initialize() {
	return true;
}
bool MainState::Update(const float &p_deltatime) {
	return false;
}
void MainState::Draw() {
	m_gui_manager->Draw(m_window);
}
void MainState::Shutdown() {
	delete m_gui_manager;
	m_gui_manager = nullptr;
}

std::string MainState::Get_next() {
	return m_next;
}