#include "../stdafx.h"
#include "GameState.h"

GameState::GameState(sf::RenderWindow *p_window, TextureManager *p_texture_manager) {
	m_window = p_window;
	m_texture_manager = p_texture_manager;
	m_gui_manager = new GuiManager();
}
GameState::~GameState(void) {
}

bool GameState::Initialize() {

	m_texture_manager = new TextureManager();
	m_sprite_manager = new SpriteManager();
	m_sprite_manager->Initialise("../Data/Sprites/");

	m_box2d_manager = new Box2DManager();
	m_box2d_manager->Initialize();

	//Player
	m_player = new Player(m_window, m_sprite_manager);
	m_player->SetBody(m_box2d_manager->CreateDynamicBody(m_player->GetPosition().x,
		-m_player->GetPosition().y,
		50/2, 50/2));

	//Walls
	m_wall_manager = new Walls(m_box2d_manager);

	//Left wall
	m_wall_manager->AddWall(sf::Vector2f(-50.0f, 0.0f), 50, 720);
	//Upper Wall
	m_wall_manager->AddWall(sf::Vector2f(0.0f, -50.0f), 1280, 50);
	//Right Wall
	m_wall_manager->AddWall(sf::Vector2f(1330.0f, 0.0f), 50, 720);
	//Lower Wall
	m_wall_manager->AddWall(sf::Vector2f(0.0f, 775.0f), 1280, 50);


	//Flock Manager
	m_flock_manager = new FlockManager(m_window, m_sprite_manager, m_box2d_manager, m_player);

	for (float x = 0.0f; m_flock_manager->m_sheep.size() < 18; x+=101)
	{
		for (float y = 0.0f; y < 255; y+=101)
		{
			m_flock_manager->AddSheep(sf::Vector2f(x + 605.0f, y + 300.0f), "Blue Sheep");
			m_flock_manager->AddSheep(sf::Vector2f(x + 300.0f, y + 300.0f), "Pink Sheep");
		}
	}
	return true;
}
bool GameState::Update(const float &p_deltatime) {
	m_box2d_manager->Update(p_deltatime);
	m_player->Update(p_deltatime);
	m_flock_manager->Update(p_deltatime);
	return false;
}
void GameState::Draw() {
	m_gui_manager->Draw(m_window);
	m_flock_manager->Draw();
	m_player->Draw();
}
void GameState::Shutdown() {
	delete m_gui_manager;
	m_gui_manager = nullptr;

	delete m_texture_manager;
	m_texture_manager = nullptr;

	delete m_sprite_manager;
	m_sprite_manager = nullptr;

	delete m_flock_manager;
	m_flock_manager = nullptr;

	delete m_player;
	m_player = nullptr;

	delete m_wall_manager;
	m_wall_manager = nullptr;

	delete m_box2d_manager;
	m_box2d_manager = nullptr;
}

std::string GameState::Get_next() {
	return m_next;
}