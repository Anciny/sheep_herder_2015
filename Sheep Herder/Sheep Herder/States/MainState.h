#pragma once

#include "State.h"

#include "../Managers/TextureManager.h"
#include "../Managers/GuiManager.h"

class MainState : public State {
public:
	MainState(sf::RenderWindow *window, TextureManager *texture_manager);
	~MainState(void);

	bool Initialize();
	bool Update(const float &deltatime);
	void Draw();
	void Shutdown();

	std::string Get_next();
private:
private:
	sf::RenderWindow *m_window;

	TextureManager *m_texture_manager;
	GuiManager *m_gui_manager;

	std::string m_next;
};