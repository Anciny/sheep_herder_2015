#pragma once

#include "../stdafx.h"

class State {
public:
	virtual bool Initialize() = 0;
	virtual bool Update(const float &deltatime) = 0;
	virtual void Draw() = 0;
	virtual void Shutdown() = 0;

	virtual std::string Get_next() = 0;
};