#pragma once

#include "State.h"

#include "../Managers/TextureManager.h"
#include "../Managers/GuiManager.h"
#include "../SpriteManager.h"
#include "../FlockManager.h"
#include "../Box2DManager.h"
#include "../Player.h"
#include "../Math.h"
#include "../Walls.h"


class GameState : public State {
public:
	GameState(sf::RenderWindow *window, TextureManager *texture_manager);
	~GameState(void);

	bool Initialize();
	bool Update(const float &deltatime);
	void Draw();
	void Shutdown();

	std::string Get_next();

private:
	sf::RenderWindow *m_window;

	TextureManager *m_texture_manager;
	GuiManager *m_gui_manager;

	Box2DManager *m_box2d_manager;
	FlockManager *m_flock_manager;
	SpriteManager *m_sprite_manager;

	Player *m_player;

	Walls *m_wall_manager;

	std::string m_next;
};