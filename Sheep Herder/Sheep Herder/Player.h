#pragma once

class Player {

public:
	Player(sf::RenderWindow *, SpriteManager* sprite_manager);
	~Player(void);

	void Update(float deltatime);
	void Draw();

	sf::Sprite* GetCurrentSprite();
	void AddSprite(std::string sprite_name, Sprite* player_sprite);

	void Move(b2Vec2 linearvelocity);
	sf::Vector2f GetPosition();

	b2Body* GetBody();
	void SetBody(b2Body* newbody);


private:

	//sf::Sprite m_sprite;
	//sf::Texture m_texture;

	SpriteManager* m_sprite_manager;

	Sprite* m_current_sprite;
	std::map<std::string, Sprite*> m_sprites;
	std::string m_player_sprite;
	std::string m_sprite_name;

	sf::RenderWindow *m_window;

	b2Body* m_body;

	sf::Vector2f m_position;

	int m_width;
	int m_height;

	float RotatePlayer();
	float m_speed; 

	b2Vec2 m_velocity;
};

