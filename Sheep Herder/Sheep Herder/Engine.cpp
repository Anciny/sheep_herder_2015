#include "stdafx.h"
#include "Engine.h"

Engine::Engine() {
}
Engine::~Engine() {
}

void Engine::Initialize() {
	m_window = new sf::RenderWindow(sf::VideoMode(1280, 720), "Sheep Herder Simulator 2015");
	m_window->setFramerateLimit(500);

	m_texture_manager = new TextureManager();
	m_state_manager = new StateManager(m_window, m_texture_manager);
	
	m_state_manager->Set_state("Game");
	m_time = m_clock.restart();
}

void Engine::Run() {
	while (m_window->isOpen()) {
		sf::Event evt;
		m_deltatime = (float)m_clock.restart().asMilliseconds();
		while (m_window->pollEvent(evt)) {
			if (evt.type == sf::Event::Closed)
				m_window->close();
		}
		//m_time = m_clock.restart();
		if (m_state_manager->Update(m_time.asSeconds()))
			m_window->close();
		else {
			m_window->clear(sf::Color::White);
			m_state_manager->Draw();
			m_window->display();
		}
		m_time = m_clock.restart();
	}
}
void Engine::Shutdown() {
	delete m_texture_manager;
	m_texture_manager = nullptr;

	delete m_state_manager;
	m_state_manager = nullptr;

	delete m_window;
	m_window = nullptr;
}