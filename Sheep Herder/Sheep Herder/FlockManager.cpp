// FlockManager.cpp

#include "stdafx.h"
#include <iostream>
#include "FlockManager.h"
#include "Math.h"


FlockManager::FlockManager(sf::RenderWindow *setWindow, SpriteManager* sprite_manager, Box2DManager* box2dmanager, Player* player)
{
	m_sprite_manager = sprite_manager;
	m_box2d_manager = box2dmanager;
	m_window = setWindow;

	m_width = 50;
	m_height = 50;

	m_blue_sprite = "BlueSheep.png";
	m_pink_sprite = "PinkSheep.png";
	m_player = player;

	alignmentWeight = 2.0f;
	cohesionWeight = 5.0f;
	separationWeight = 5.0f;
	fleeWeight = 20.0f;
}

FlockManager::~FlockManager(void)
{
	for (int i = 0; i < m_sheep.size(); i++)
	{
		m_sheep[i]->Cleanup();
		delete m_sheep[i]; 
		m_sheep[i] = nullptr;
		m_sheep.erase(m_sheep.begin() + i);
	}
}

void FlockManager::Update(float deltatime)
{
	for (int i = 0; i < m_sheep.size(); i++)
	{
		Flocking(m_sheep[i]);

		m_sheep[i]->ALittleMovement();

		m_sheep[i]->Move();
		m_sheep[i]->Update(deltatime);
	}
}

void FlockManager::Draw()
{
	for (int i = 0; i < m_sheep.size(); i++)
	{
		m_window->draw(*m_sheep[i]->GetCurrentSprite());
	}
}

void FlockManager::AddSheep(sf::Vector2f position, std::string sheep_color)
{
	std::string bluesheep = "Blue Sheep";
	std::string pinksheep = "Pink Sheep";
	if (sheep_color == "Blue Sheep")
	{
		m_sheep.push_back(new Sheep(position, m_width, m_height, "blue"));
		m_sheep[m_sheep.size() - 1]->AddSprite(bluesheep, m_sprite_manager->Load(m_blue_sprite, m_width, m_height));
		m_sheep[m_sheep.size() - 1]->SetPosition(position);
		m_sheep[m_sheep.size() - 1]->SetBody(m_box2d_manager->CreateDynamicBody(position.x, -position.y, m_height/2, m_width/2));
	}
	else
	{
		m_sheep.push_back(new Sheep(position, m_width, m_height, "pink"));
		m_sheep[m_sheep.size() - 1]->AddSprite(pinksheep, m_sprite_manager->Load(m_pink_sprite, m_width, m_height));
		m_sheep[m_sheep.size() - 1]->SetPosition(position);
		m_sheep[m_sheep.size() - 1]->SetBody(m_box2d_manager->CreateDynamicBody(position.x, -position.y, m_height/2, m_width/2));
	}
}

void FlockManager::Flocking(Sheep* current_sheep)
{
	sf::Vector2f alignment = ComputeAlignment(current_sheep);
	sf::Vector2f cohesion = ComputeCohesion(current_sheep);
	sf::Vector2f separation = ComputeSeparation(current_sheep);
	sf::Vector2f flee = FleeFromPlayer(current_sheep);

	/*sf::Vector2f temp = sf::Vector2f(
	m_sheep[i]->GetVelocity().x + alignment.x + cohesion.x + separation.x,
	m_sheep[i]->GetVelocity().y + alignment.y + cohesion.y + separation.y);*/


	//Experiment with the weights to change the way agents flock
	sf::Vector2f temp = sf::Vector2f(
		alignment.x * alignmentWeight +
		cohesion.x * cohesionWeight +
		separation.x * separationWeight +
		flee.x * fleeWeight 
		,
		alignment.y * alignmentWeight +
		cohesion.y * cohesionWeight +
		separation.y * separationWeight +
		flee.y * fleeWeight 
		);

	//temp = Math::Normalize(temp);

	current_sheep->SetVelocity(temp);
}

void FlockManager::Move(sf::Vector2f movement)
{

}

sf::Vector2f FlockManager::ComputeAlignment(Sheep* thisSheep)
{
	sf::Vector2f v = sf::Vector2f(0, 0); //store vector we'll compute
	int neighbourCount = 0;

	Sheep* agent;

	for each (agent in m_sheep)
	{
		if (agent != thisSheep)
		{
			if (Math::GetDistance(thisSheep->GetPosition(), agent->GetPosition()) < m_group_distance)
			{
				v.x += agent->GetVelocity().x;
				v.y += agent->GetVelocity().y;
				neighbourCount++;
			}
		}
	}

	if (neighbourCount == 0)
	{
		return v;
	}

	v.x /= neighbourCount;
	v.y /= neighbourCount;
	v = Math::Normalize(v);
	return v;
}

sf::Vector2f FlockManager::ComputeCohesion(Sheep* thisSheep)
{
	sf::Vector2f v = sf::Vector2f(0, 0); //store vector we'll compute
	int neighbourCount = 0;

	Sheep* agent;

	float f1 = ((m_group_distance - m_personal_space) / 2);
	float f2 = (m_personal_space + f1);

	for each (agent in m_sheep)
	{
		if (agent != thisSheep)
		{
			float distance = Math::GetDistance(thisSheep->GetPosition(), agent->GetPosition());
			float temp = ((distance - f2) / f1);
			if (distance < m_group_distance && temp >=0)
			{
				v.x += (agent->GetPosition().x - thisSheep->GetPosition().x) * temp;
				v.y += (agent->GetPosition().y - thisSheep->GetPosition().y) * temp;
				neighbourCount++;
			}
		}
	}

	if (v.x < 0)
	{
		v.x = 0.0f;
	}
	if (v.y < 0)
	{
		v.y = 0.0f;
	}

	if (neighbourCount == 0)
		return v;

	v.x /= neighbourCount;
	v.y /= neighbourCount;
	sf::Vector2f vcenter = sf::Vector2f(v.x - thisSheep->GetPosition().x, v.y - thisSheep->GetPosition().y);
	vcenter = Math::Normalize(vcenter);
	return vcenter;
}

sf::Vector2f FlockManager::ComputeSeparation(Sheep* thisSheep)
{
	sf::Vector2f v = sf::Vector2f(0, 0); //store vector we'll compute
	int neighbourCount = 0;

	Sheep* agent;

	for each (agent in m_sheep)
	{
		if (agent != thisSheep)
		{
			float distance = Math::GetDistance(thisSheep->GetPosition(), agent->GetPosition());
			if (distance < m_group_distance)
			{
				v.x += (agent->GetPosition().x - thisSheep->GetPosition().x) * (distance / m_personal_space);
				v.y += (agent->GetPosition().y - thisSheep->GetPosition().y) * (distance / m_personal_space);
				neighbourCount++;
			}
		}
	}

	if (neighbourCount == 0)
	{
		return v;
	}

	v.x *= -1;
	v.y *= -1;
	//v.x /= neighbourCount;
	//v.y /= neighbourCount;
	v = Math::Normalize(v);
	return v;
}

sf::Vector2f FlockManager::FleeFromPlayer(Sheep* current_sheep)
{
	sf::Vector2f v = sf::Vector2f(0, 0);

	if (Math::GetDistance(current_sheep->GetPosition(), m_player->GetPosition()) < 200)
	{
		v.x += (m_player->GetPosition().x - current_sheep->GetPosition().x);
		v.y += (m_player->GetPosition().y - current_sheep->GetPosition().y);
	}

	v.x *= -1;
	v.y *= -1;
	v = Math::Normalize(v);
	return v;
}