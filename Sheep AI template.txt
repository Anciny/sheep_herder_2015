Update observation
	Observe local herd
		Set nearest sheep cluster
		or
		Set local herd
	Observe dogs
		Set position
		Set direction
		Set speed
	Feel dog threat // Closer dogs are at a higher threat level
	Feel comfort // Higher depending on amount of nearby sheep
	Feel edginess // Cannot be lower than highest threat, lowers each update with comfort
Update planning
	Calculate speed // Speed increases with edginess
	Calculate flee direction
		Calculate dog alignment // Sheep have same direction as dog if dog does not have a higher speed than sheep
		Calculate dog tangent // Sheep flee in the direction of the tangent to the dogs direction if the dog is sprinting
	Calculate local herd alignment // Sheep try to have same direction as the rest of the nearby sheep
	Calculate local herd cohesion // Sheep try to stay close to the nearby sheep
	Calculate final movement direction // Flee direction increases with threat, alignment and cohesion decreases with edginess
Update action
	Check collision with movement and speed
	Move with movement and new speed